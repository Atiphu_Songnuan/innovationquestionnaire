<header class="main-header">

    <!-- <nav class="navbar navbar-expand  topbar mb-4 static-top shadow" style="background: #2193b0;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to bottom, #6dd5ed, #2193b0);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to bottom, #6dd5ed, #2193b0); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    "> -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow" style="background-image: linear-gradient(-225deg, #E3FDF5 0%, #FFE6FA 100%);">
        <!-- Sidebar Toggle (Topbar) -->
        <!-- <button id="sidebarToggleTop" class="btn btn-link d-md-none round-circle mr-3">
            <i class="fa fa-bars"></i>
        </button> -->
        <h5>นวัตกรรมทางการแพทย์<br><small>คณะแพทยศาสตร์ ม.อ. ครั้งที่ 1</small></h5>
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle" src="./public/img/man.svg">
            </a>
            </li>
        </ul>
    </nav>
</header>

<script>
    function Logout(){
        Swal.fire({
        title: 'ต้องการออกจากระบบหรือไม่?',
        // text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ใช่',
        cancelButtonText: 'ยกเลิก'
        }).then((result) => {
        if (result.value) {
            $.ajax({
            type:"POST",
            url:"../medcheer/ApiService/SessionDestroy",
            success: function() {
                Swal.fire({
                title: 'ออกจากระบบสำเร็จ',
                text: '',
                type: 'success',
                timer: 2000,
                showConfirmButton: false,
                onClose: () =>{
                    window.location.href='../medcheer/login';
                }
                });
            },
            error: function() {
                console.log('Error occured');
            }
            });
        }
        });
    }
</script>
