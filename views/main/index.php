<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>questionnaire</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link href="./public/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- <link href="./public/css/qrcode/qrcode.min.css" rel="stylesheet">
    <link href="./public/css/qrcode/qrcodestyle.css" rel="stylesheet"> -->

    <!--===============================================================================================-->
      <link rel="icon" type="image/png" href="./public/img/lamp.ico"/>
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="./public/vendor/login/css/util.css">
      <!-- <link rel="stylesheet" type="text/css" href="./public/vendor/login/css/main.css"> -->
    <!--===============================================================================================-->

    <!-- SweetAlert2 -->
      <link rel="stylesheet" href="./public/vendor/sweetalert2/dist/sweetalert2.min.css">
    <!-- Google Font -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
    <link href="https://fonts.googleapis.com/css?family=Lobster|Poppins|Kanit" rel="stylesheet">

    <style>
      .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }
        div.dataTables_info , a{
            font-size: 14px;
        }
        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            vertical-align: middle;
            width: 1%;
        }

        .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        td{
            white-space:nowrap;
            font-size: 14px;
            /* vertical-align: middle; */
        }
        tr{
            font-family: 'Kanit', serif;
            font-size: 16px;
        }
      label.label-data{
        font-size: 20px;
      }
      .modal{
        font-family: 'Kanit', serif;
      }
      .btn-question{
        justify-content: center;
        align-items: center;
        padding: 0 20px;
        width: 100%;
        height: 60px;
        /* background-color: #3b5998; */
        border-radius: 10px;

        font-family: 'Kanit', serif;
        font-size: 18px;
        color: #fff;
        line-height: 1.2;

        -webkit-transition: all 0.4s;
        -o-transition: all 0.4s;
        -moz-transition: all 0.4s;
        transition: all 0.4s;
        position: relative;
        z-index: 1;
      }
      .btn-question::before{
        content: "";
        color:#fff;
        display: block;
        position: absolute;
        z-index: -1;
        width: 100%;
        height: 100%;
        border-radius: 10px;
        top: 0;
        left: 0;
        background: #a64bf4;
        background: -webkit-linear-gradient(45deg, #00dbde, #fc00ff);
        background: -o-linear-gradient(45deg, #00dbde, #fc00ff);
        background: -moz-linear-gradient(45deg, #00dbde, #fc00ff);
        background: linear-gradient(45deg, #00dbde, #fc00ff);
        opacity: 0;
        -webkit-transition: all 0.4s;
        -o-transition: all 0.4s;
        -moz-transition: all 0.4s;
        transition: all 0.4s;
      }

      .btn-question:hover:before {
        opacity: 1;
        color:#fff;
      }

      .swal2-popup {
        font-family: 'Kanit', serif;
        font-size: 0.7rem !important;
      }

      .background-container{

        /* width: 100%;   */
        min-height: 100vh;
        /* display: -webkit-box; */
        /* display: -webkit-flex; */
        /* display: -moz-box; */
        /* display: -ms-flexbox; */
        /* display: flex; */
        /* flex-wrap: wrap; */
        justify-content: center;
        align-items: center;
        padding-top: 50px;
        padding-bottom: 50px;
        
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
      }

    </style>
  </head>
  <body>
    <div class="limiter">
      <div class="background-container" style="background-image: url('./public/vendor/login/images/green.png');">
        <div class="p-l-110 p-r-110 p-t-30 p-b-30 m-l-30 m-r-30" style="background: #fff; border-radius: 10px; padding-left: 15px;padding-right: 15px;">
            <!-- PSU Logo -->
            <!-- <div class="col-md-12 text-right">
              <img src="./public/img/psu_logo.png" width="40" height="30" alt="logo">
            </div> -->
          <div class="row">
            <div class="col-md-12 text-center">
              <img src="./public/img/lamp.svg" width="80" height="80" alt="logo">
            </div>

            <div class="col-md-12 text-center">
              <img src="./public/img/psu_logo.png" width="55" height="40" alt="logo">              
            </div>
            <div class="col-md-12 m-t-5 text-center">
              <span style="font-family: 'Kanit', serif; font-size:26px">
                ประชุมวิชาการนวัตกรรมทางการแพทย์<br><small style="font-size: 16px">คณะแพทยศาสตร์ ม.อ. ครั้งที่ 1</small>
              </span>
            </div>

            <div class="col-md-12 m-t-30 m-b-15">
              <span style="text-align:left; font-size: 18px; font-family: 'Kanit', serif;">
              <i class="fas fa-calendar-alt"></i> วันพุธที่ 13 มีนาคม 2562 ประชุมเชิงปฏิบัติการ Design Thinking for Product Innovation ณ ห้องประชุมชั้น 12 อาคารเฉลิมพระบารมี โรงพยาบาลสงขลานครินทร์ (สาหรับผู้ลงทะเบียนเข้าร่วมประชุมเชิงปฏิบัติการ)
              </span>
            </div>

            <div class="col-md-12">
              <div class="table-responsive m-b-30">
                <table class="table table-bordered" id="determinetable1" width="100%" cellspacing="0">
                  <thead>
                      <tr class="bg bg-info text-white">
                          <th>เวลา</th>
                          <th>หัวข้อ</th>
                          <th>วิทยากร</th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">08.30 – 09.00 น.</span></td>
                      <td>ลงทะเบียน</td>
                      <td></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">09.00 – 10.00 น.</span></td>
                      <td>Concept of Design Thinking</td>
                      <td>ผศ.นพ.สิทธิโชค อนันตเสรี</td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">10.00 – 10.30 น.</span></td>
                      <td>Empathy จุดประกายเริ่มต้นสร้างสรรค์นวัตกรรม และฝึกปฏิบัติการทาความเข้าใจและค้นหาปัญหา<br>และความต้องการของกลุ่มเป้าหมายอย่างรอบด้าน</td>
                      <td>ผศ.นพ.สิทธิโชค อนันตเสรี</td>
                    </tr>
                    
                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">10.30 – 10.45 น.</span></td>
                      <td class="bg bg-success text-white" style="font-weight:bold">พักรับประทานอาหารว่าง</td>
                      <td class="bg bg-success text-white"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">10.45 – 12.00 น.</span></td>
                      <td>Define and Ideate การวิเคราะห์ข้อมูลของปัญหาและความต้องการของกลุ่มเป้าหมาย<br>เพื่อระบุถึงแก่นของปัญหา (pain point) และสืบค้นความต้องการที่แท้จริง (gain point) ในทุกมุมมองของกลุ่มเป้าหมาย<br>เป็นข้อมูลสู่กระบวนการระดมสมองคิดสร้างสรรค์ออกแบบและสร้างแนวคิดของชิ้นงานที่ตอบโจทย์ได้ตรงจุด</td>
                      <td>ผศ.นพ.สิทธิโชค อนันตเสรี<br>ผศ.ดร.สุรพงษ์ ชาติพันธุ์</td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">12.00 – 13.00 น.</span></td>
                      <td class="bg bg-success text-white" style="font-weight:bold">พักรับประทานอาหารกลางวัน</td>
                      <td class="bg bg-success text-white"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">13.00 – 14.30 น.</span></td>
                      <td>Patent search and Prototype จากแนวคิดการออกแบบสู่การพิสูจน์ความใหม่ด้วยการสืบค้นสิทธิบัตรที่มีมาก่อน <br>และสนุกกับการลงมือสร้างชิ้นงานต้นแบบอย่างง่าย เพื่อทดสอบและพิสูจน์แนวคิดสร้างสรรค์ที่สามารถจับต้องได้จริง</td>
                      <td>ผศ.นพ.สิทธิโชค อนันตเสรี<br>ผศ.ดร.สุรพงษ์ ชาติพันธุ์</td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">14.30 – 14.45 น.</span></td>
                      <td class="bg bg-success text-white" style="font-weight:bold">พักรับประทานอาหารว่าง</td>
                      <td class="bg bg-success text-white"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">14.45 – 15.45 น.</span></td>
                      <td>Test การนาเสนอผลงาน แลกเปลี่ยนเรียนรู้ เพื่อรับฟังข้อมูลย้อนกลับ เป็นแนวทางที่จะพัฒนานวัตกรรมให้ดีมากยิ่งขึ้น</td>
                      <td>ผศ.นพ.สิทธิโชค อนันตเสรี</td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">15.45 – 16.00 น.</span></td>
                      <td class="bg bg-warning" style="font-weight:bold">Q & A</td>
                      <td class="bg bg-warning"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <button class="form-control btn-question bg bg-primary m-b-30" onclick="GoToQuestionnaire1()" style="cursor:pointer; font-family: 'Kanit', serif; box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); border-radius: 10px; text-align: center;"><i class="fas fa-question-circle"></i> แบบประเมิน วันที่ 13 มี.ค. 62</button>
            </div>

            <div class="col-md-12 m-t-30 m-b-15">
              <span style="text-align:left; font-size: 18px; font-family: 'Kanit', serif;">
              <i class="fas fa-calendar-alt"></i> วันพฤหัสที่ 14 มีนาคม พ.ศ. 2562 ประชุมวิชาการ หัวข้อ “Innovation In Action” ณ ห้องประชุมชั้น 12 อาคารเฉลิมพระบารมี โรงพยาบาลสงขลานครินทร์
              </span>
            </div>

            <div class="col-md-12">
              <div class="table-responsive m-b-30">
                <table class="table table-bordered" id="determinetable2" width="100%" cellspacing="0">
                  <thead>
                      <tr class="bg bg-info text-white">
                          <th>เวลา</th>
                          <th>หัวข้อ</th>
                          <th>วิทยากร</th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">09.00 – 09.15 น.</span></td>
                      <td>พิธีเปิด : คณบดีกล่าวเปิดงาน โดย รศ.นพ.พุฒิศักดิ์ พุทธวิบูลย์ คณบดีคณะแพทยศาสตร์</td>
                      <td></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">09.15 – 10.15 น.</span></td>
                      <td>Innovation in Action</td>
                      <td>ผศ.นพ.สิทธิโชค อนันตเสรี</td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">10.15 – 10.30 น.</span></td>
                      <td class="bg bg-success text-white" style="font-weight:bold">พักรับประทานอาหารว่าง</td>
                      <td class="bg bg-success text-white"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">10.30 – 12.00 น.</span></td>
                      <td>HealthTech Innovation <br>* Smart Hospital <br>* My Health care <br>* Home Health care</td>
                      <td>นพ.กรกช มะลิวรรณกุล<br>นพ.เอกทิตย์ กู้ไพบูลย์,<br>ผศ.ดร.วรรณรัช สันติอมรทัต,<br>Moderator : ผศ.ดร.สุรพงษ์ ชาติพันธุ์</td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">12.00 – 13.00 น.</span></td>
                      <td class="bg bg-success text-white" style="font-weight:bold">พักรับประทานอาหารกลางวัน</td>
                      <td class="bg bg-success text-white"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">13.00 – 14.30 น.</span></td>
                      <td class="bg bg-warning">เยี่ยมชมผลงานประกวดนวัตกรรม</td>
                      <td class="bg bg-warning"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">14.30 – 14.45 น.</span></td>
                      <td class="bg bg-success text-white" style="font-weight:bold">พักรับประทานอาหารว่าง</td>
                      <td class="bg bg-success text-white"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">14.45 – 16.15 น.</span></td>
                      <td class="bg bg-warning"><span style="font-size:18px">Innovation show cases /Q & A</span><br>* อุปกรณ์ครอบปากและฟัน<br>* เตียงถ่ายภาพรังสีทารก (Newborn X-ray Bed)<br>* หุ่นตังกวน สาหรับฝึกช่วยชีวิต ทาจากยางพารา<br>* ระบบคอมพิวเตอร์สาหรับการตรวจวินิจฉัยตาเขและการดูแล<br>* อุปกรณ์ฝึกการยืนสาหรับเด็กเล็กที่มีพัฒนาการช้า<br>* เครื่องเอกซเรย์คอมพิวเตอร์สามมิติขนาดเล็ก MiniiScan<br>* Smart Party 2019</td>
                      <td class="bg bg-warning"></td>
                    </tr>

                  </tbody>
                </table>
              </div>
            </div>
            
            <div class="col-md-12 m-t-30 m-b-15">
              <span style="text-align:left; font-size: 18px; font-family: 'Kanit', serif;">
              <i class="fas fa-calendar-alt"></i> วันศุกร์ที่ 15 มีนาคม พ.ศ. 2562
              </span>
            </div>

            <div class="col-md-12">
              <div class="table-responsive m-b-30">
                <table class="table table-bordered" id="determinetable3" width="100%" cellspacing="0">
                  <thead>
                      <tr class="bg bg-info text-white">
                          <th>เวลา</th>
                          <th>หัวข้อ</th>
                          <th>วิทยากร</th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">09.00 – 10.15 น.</span></td>
                      <td><span style="font-size:18px">Innovation Morning Talk :</span><br>* คิดนอกกรอบ ออกแบบนวัตกรรม<br>* สิทธิบัตร สาคัญไฉน?<br>* สร้างต้นแบบด้วย 3D Printing</td>
                      <td>ผศ.ดร.สุรพงษ์ ชาติพันธุ์ <br> ผศ.ดร.ภัทร อัยรักษ์ <br>คุณนิติ เพชรสุวรรณ <br>Moderator : ผศ.พญ.นลินี โกวิทวนาวงษ์</td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">10.15 – 10.30 น.</span></td>
                      <td class="bg bg-success text-white" style="font-weight:bold">พักรับประทานอาหารว่าง</td>
                      <td class="bg bg-success text-white"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">10.30 – 12.00 น.</span></td>
                      <td class="bg bg-warning"><span style="font-size:18px">Innovation Contest : </span><br>นาเสนอผลงานประกวดนวัตกรรม 5 ทีม ที่ได้รับการคัดเลือกนาเสนอบนเวที</td>
                      <td class="bg bg-warning"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">12.00 – 13.00 น.</span></td>
                      <td class="bg bg-success text-white" style="font-weight:bold">พักรับประทานอาหารกลางวัน</td>
                      <td class="bg bg-success text-white"></td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">13.00 – 13.30 น.</span></td>
                      <td>Funding for Innovation</td>
                      <td>ผศ.พญ.นลินี โกวิทวนาวงษ์</td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">13.30 – 14.30 น.</span></td>
                      <td>Robotics in Healthcare</td>
                      <td>รศ.ดร.พฤทธิกร สมิตไมตรี</td>
                    </tr>

                    <tr>
                      <td class="text-center"><span class="badge badge-pill badge-danger" style="font-size:14px;">14.30 – 15.00 น.</span></td>
                      <td class="bg bg-warning">มอบรางวัล พิธีปิด</td>
                      <td class="bg bg-warning"></td>
                    </tr>

                  </tbody>
                </table>
              </div>
              <button class="form-control btn-question bg bg-primary m-b-30" onclick="GoToQuestionnaire2()" style="cursor:pointer; font-family: 'Kanit', serif; box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); border-radius: 10px; text-align: center;"><i class="fas fa-question-circle"></i> แบบประเมิน วันที่ 14-15 มี.ค. 62</button>
            </div>

            <div class="col-md-12">
              <button class="form-control btn-question m-b-30" onclick="GoToEbook()" style="cursor:pointer; background-color:#dc3545; font-family: 'Kanit', serif; box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); border-radius: 10px; text-align: center;"><i class="fas fa-book"></i> E-Book</button>
            </div>
          </div>
        </div>
          <!-- <button class="form-control btn-question m-b-30" onclick="GoToQuestionnaire()" style="font-family: 'Kanit', serif; box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); border-radius: 10px; text-align: center;"><i class="fas fa-question-circle"></i> แบบประเมิน วันที่ 13 มี.ค. 62</button>
          <button class="form-control btn-question" onclick="GoToEbook()" style="background-color:#dc3545; font-family: 'Kanit', serif; box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); border-radius: 10px; text-align: center;"><i class="fas fa-book"></i> E-book</button> -->
      </div>
    </div>


    <!-- <script src="./public/vendor/jquery/jquery.min.js"></script> -->

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!-- Datatable -->
      <script src="./public/vendor/datatables/jquery.dataTables.js"></script>
      <script src="./public/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Demo scripts for this page-->
      <script src="./public/js/datatables-demo.js"></script>

    <!-- SweetAlert2 -->
      <script src="./public/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

    <script>
      $(function(){
        $('#determinetable1').dataTable({
          // "order": [[ 0, "desc" ]],
          "searching": false,
          "bPaginate": false,
          "bFilter": true,
          "bInfo": false,
          "bAutoWidth": false,
          "bLengthChange": false
          // "columnDefs": [
          //   {
          //     "targets": [ 0 ],
          //     "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
          //     "searchable": false
          //   }
          // ]
        });

        $('#determinetable2').dataTable({
          // "order": [[ 0, "desc" ]],
          "searching": false,
          "bPaginate": false,
          "bFilter": true,
          "bInfo": false,
          "bAutoWidth": false,
          "bLengthChange": false
          // "columnDefs": [
          //   {
          //     "targets": [ 0 ],
          //     "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
          //     "searchable": false
          //   }
          // ]
        });

        $('#determinetable3').dataTable({
          // "order": [[ 0, "desc" ]],
          "searching": false,
          "bPaginate": false,
          "bFilter": true,
          "bInfo": false,
          "bAutoWidth": false,
          "bLengthChange": false
          // "columnDefs": [
          //   {
          //     "targets": [ 0 ],
          //     "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
          //     "searchable": false
          //   }
          // ]
        });
      });

      function GoToQuestionnaire1(){
        window.open('https://docs.google.com/forms/d/e/1FAIpQLScsKjbkb_QgIJ4KgCuBm0j1Qbgd9dhkmHnhef_MPU-4vqVSUQ/viewform', '_blank'); 
      }

      function GoToQuestionnaire2(){
        window.open('https://docs.google.com/forms/d/e/1FAIpQLSd95zESiSkvoFCna7w1vU6S_IoZBQqHd_4vg6Pd3nvlJqALGg/viewform', '_blank'); 
      }

      function GoToEbook(){
        // window.open('http://inno.medicine.psu.ac.th/', '_blank'); 
        window.location.href = './public/document/MEDPSU_1st_Innovation_In_Action_2019.pdf';
        
      }
    </script>
  </body>
</html>
